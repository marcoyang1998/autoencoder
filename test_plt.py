import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
from PIL import Image
from keras.backend import eval
from keras.layers import Reshape
from keras.models import Model, load_model

autoencoder = load_model('cnn_dense_autoencoder.h5')

def encoder(autoencoder, input_data):
    input_data = tf.convert_to_tensor(input_data, dtype=tf.float32)
    x = autoencoder.layers[0](input_data)
    x = autoencoder.layers[1](x)
    x = autoencoder.layers[2](x)
    x = autoencoder.layers[3](x)
    x = autoencoder.layers[4](x)
    x = autoencoder.layers[5](x)
    x = autoencoder.layers[6](x)
    x = autoencoder.layers[7](x)
    return x


def encoder_filters(autoencoder):
    return autoencoder.layers[3].get_weights()

filters = autoencoder.layers[3].get_weights()
#print(type(autoencoder.layers[3].get_weights()))
filters = np.ndarray(filters, np.float32)
print(filters.shape)

plt.figure(figsize=(15, 10))

bild = Image.open('eifel.png').convert('L')
bild = np.array(bild.getdata(), np.float32)
#print(bild)
bild = np.reshape(bild, [28, 28])

encoded = encoder(autoencoder, bild.reshape([1, 28, 28, 1]))
encoded = Reshape([7, 7, 16])(encoded)
encoded = eval(encoded)
#print(encoded.shape)
encoded = np.reshape(encoded, [7, 7, 16])
for i in range(16):
    x = encoded[:, :, i]
    plt.subplot(1, 16, i+1)
    plt.imshow(x.reshape([7, 7]))
    plt.gray()

plt.show()

output_bild = autoencoder.predict(bild.reshape([1, 28, 28, 1]))
#print(output_bild)
plt.subplot(211)
plt.imshow(bild.reshape([28, 28]))
plt.title('input image')
plt.gray()

ax = plt.subplot(212)
plt.imshow(output_bild.reshape([28, 28]))
plt.title('output image')
plt.gray()



plt.show()

