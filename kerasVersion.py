import tensorflow as tf
import matplotlib.pyplot as plt
import os
import scipy.misc
from keras.datasets import mnist
from keras.models import Model
from keras.utils import np_utils
from tensorflow import keras
from keras.layers import Dense, Flatten, Input, Conv2D, Conv2DTranspose, MaxPooling2D, UpSampling2D, BatchNormalization, Reshape
from PIL import Image
from keras.utils import to_categorical
import numpy as np

def add_noise(x_train, number):
    for index in range(number):
        noise = np.random.normal(0, 25, 28*28).astype('float32')
        noise = np.reshape(noise, [28, 28, 1])
        x_train[index] += noise

    x_train[x_train < 0 ] = 0
    x_train[x_train > 255] = 255

    return x_train



img_x, img_y = 28,28

(x_train, y_train), (x_test, y_test) = mnist.load_data()  # x: image  y: corresponding label
#print(x_train[0])
#print(y_train[1])

y_train = to_categorical(y_train)   # convert label to a row of binary numbers
y_test = to_categorical((y_test))


x_train = x_train.reshape(x_train.shape[0], 28, 28, 1).astype('float32')
x_test = x_test.reshape(x_test.shape[0], 28, 28, 1).astype('float32')
x_train_noised = add_noise(x_train.copy(), 60000).astype('float32')
x_test_noised = add_noise(x_test.copy(), 10000).astype('float32')

#plt.imshow(x_train_noise[0].reshape([28, 28]))
#plt.gray()
#plt.show()


input_shape = (img_x, img_y, 1)

x_train = x_train.astype('float32')/255.
x_test = x_test.astype('float32')/255.
x_train_noised = np.reshape(x_train_noised, [-1, 28, 28, 1])/255.
x_test_noised = np.reshape(x_test_noised, [-1, 28, 28, 1])/255.


input_layer = Input(input_shape)

# encoder part

x = Conv2D(64, (5, 5), activation='relu', padding='same')(input_layer)
x = MaxPooling2D((2, 2), padding='same')(x)
x = Conv2D(32, (5, 5), activation='relu', padding='same')(x)
x = MaxPooling2D((2, 2), padding='same')(x)
x = Conv2D(32, (5, 5), activation='relu', padding='same')(x)
# = Flatten()(x)
#x = Dense(128, activation='relu')(x)
#x = Dense(64, activation='relu')(x)
encoded = x


#x = Dense(128, activation='relu')(encoded)
#x = tf.reshape(x, [-1, 7, 7, 8])
x = Conv2D(32, (5, 5), activation='relu', padding='same')(x)
x = UpSampling2D((2, 2))(x)
x = Conv2D(16, (5, 5), activation='relu', padding='same')(x)
x = UpSampling2D((2, 2))(x)
decoded = Conv2D(1, (5, 5), activation='sigmoid', padding='same')(x)



model = Model(input_layer, decoded)
model.summary()

model.compile(loss='mean_absolute_error', optimizer='adam')

print(x_train.shape)

model.fit(x_train_noised, x_train,
                epochs=50,
                batch_size=256,
                shuffle=True)
model.save("denoiser.h5")


decoded_imgs = model.predict(x_test_noised)


n = 10
plt.figure(figsize=(20, 4))
for i in range(n):
    # display original
    ax = plt.subplot(2, n, i + 1)
    plt.imshow(x_test_noised[i].reshape(28, 28))
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

    # display reconstruction
    ax = plt.subplot(2, n, i + 1 + n)
    plt.imshow(decoded_imgs[i].reshape(28, 28).astype('float32'))
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
plt.show()
