import tensorflow as tf
from keras.datasets import mnist
import matplotlib.pyplot as plt
import numpy as np
from keras.models import Model
from keras.layers import Dense, Flatten, Input, Conv2D, Conv2DTranspose, MaxPooling2D, UpSampling2D, BatchNormalization, Reshape
from PIL import Image

(x_train, y_train), (x_test, y_test) = mnist.load_data()
sample_with_noise = []
for index in range(60000):
    random_normal = np.random.normal(0, 20, [28, 28])
    new_pic = x_train[index] + random_normal
    sample_with_noise.append(new_pic)

sample_with_noise = np.reshape(sample_with_noise, [-1, 28, 28, 1])

#print(sample_with_noise.shape)
x_train = np.reshape(x_train, [-1, 28, 28, 1]).astype('float32')/255.
sample_with_noise = np.reshape(sample_with_noise, [-1, 28, 28, 1]).astype('float32')/255.


input_layer = Input([28, 28, 1])

x = Conv2D(16, (3, 3), activation='relu', padding='same')(input_layer)
x = MaxPooling2D((2, 2), padding='same')(x)
x = Conv2D(8, (3, 3), activation='relu', padding='same')(x)
x = MaxPooling2D((2, 2), padding='same')(x)
encoded = x

x = Conv2D(8, (3, 3), activation='relu', padding='same')(encoded)
x = UpSampling2D((2, 2))(x)
x = Conv2D(8, (3, 3), activation='relu', padding='same')(x)
x = UpSampling2D((2, 2))(x)
decoded = Conv2D(1, (3, 3), activation='sigmoid', padding='same')(x)

denoiser = Model(input_layer, decoded)
denoiser.summary()

denoiser.compile(loss='mean_squared_error', optimizer='adam')

#print(sample_with_noise.shape)

denoiser.fit(x_train, x_train, epochs=1, batch_size=500, shuffle=True)

denoiser.save('denoiser.n5')

length = x_test.shape[0]
test_with_noise = []
for index in range(length):
    random_normal = np.random.normal(0, 20, [28, 28])
    new_pic = x_test[index] + random_normal
    test_with_noise.append(new_pic)


x_test = np.reshape(x_test, [-1, 28, 28, 1]).astype('float32')
test_with_noise = np.array(test_with_noise, np.float32).reshape([-1, 28, 28, 1])/255.

plt.imshow(test_with_noise[0].reshape([28, 28]))
plt.show()

plt.imshow(x_test[0].reshape([28, 28]))
plt.show()

decoded_imgs = denoiser.predict(test_with_noise)

n = 10
plt.figure(figsize=(20, 4))
for i in range(n):
    # display original
    ax = plt.subplot(2, n, i + 1)
    plt.imshow(test_with_noise[i].reshape(28, 28))
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

    # display reconstruction
    ax = plt.subplot(2, n, i + 1 + n)
    plt.imshow(decoded_imgs[i].reshape(28, 28))
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
plt.show()
