import tensorflow as tf
import matplotlib.pyplot as plt
import scipy.misc
from keras.datasets import mnist
import numpy as np
from keras.models import Model, load_model
from tensorflow import keras
from keras.layers import Dense, Flatten, Input, Conv2D, Conv2DTranspose, MaxPooling2D, UpSampling2D, BatchNormalization
from PIL import Image
from keras.utils import to_categorical

classifier = load_model('cnn_classifier.n5')
(X_train, y_train), (X_test, y_test) = mnist.load_data()

#input_img = X_test[1]
#img = Image.fromarray(np.reshape(input_img,[28, 28]))
#img.show()
input_img= Image.open('4.png').convert('L')
#input_img.show()
input_img = np.array(input_img, np.uint8).astype('float32')
input_img = np.reshape(input_img, [1, 1, 28, 28])/255.
output = classifier.predict(input_img)*1000
output = np.array(output, np.uint8)
print(output)
print(np.argmax(output))