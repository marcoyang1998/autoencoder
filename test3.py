import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import scipy.misc
from keras.datasets import mnist
from keras.models import Model, load_model
from tensorflow import keras
from keras.backend import eval
from keras.layers import Dense, Flatten, Input, Conv2D, Conv2DTranspose, MaxPooling2D, UpSampling2D, BatchNormalization
from PIL import Image
from keras.utils import to_categorical


denoiser = load_model('denoiser.h5')

def encoder(denoiser, picture):
    picture = np.reshape(picture, [-1, 28, 28, 1]).astype('float32')/255.
    input = tf.convert_to_tensor(picture, dtype = tf.float32)
    x = denoiser.layers[0](input)
    x = denoiser.layers[1](x)
    x = denoiser.layers[2](x)
    x = denoiser.layers[3](x)
    x = denoiser.layers[4](x)
    encoded = x
    return encoded
pic = Image.open('yu.png').convert('L')
pic = np.array(pic.getdata(), np.float32)
#pic = np.reshape(pic, [28, 28, 1])

encoded = encoder(denoiser, pic.copy())
encoded = eval(encoded)
encoded = np.reshape(encoded, [7, 7, 32])

print(encoded.shape)
print(type(encoded))

for i in range(32):

    ax = plt.subplot(4, 8, i+1)
    plt.imshow(encoded[:,:, i].reshape([7, 7]))
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

plt.show()
