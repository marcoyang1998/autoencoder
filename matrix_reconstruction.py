import numpy as np
from keras.models import Model
from keras.layers import Dense, Flatten, Input, Conv2D, Conv2DTranspose, MaxPooling2D, UpSampling2D, BatchNormalization
from PIL import Image
import matplotlib.pyplot as plt

img_shape = [40, 40, 1]


def generate_random_matrix(num):
    input = []
    for index in range(num):
        rdm = np.random.uniform(0, 10, 40 * 40)
        rdm.astype('float32')
        rdm = np.reshape(rdm, img_shape)
        input.append(rdm)

    input = np.array(input, np.float32)
    input = np.reshape(input, [-1, 40, 40, 1])
    return input


input_data = generate_random_matrix(10000)
#print(x.shape)

input_layer = Input(img_shape)

x = Conv2D(32, (3, 3), activation='relu', padding='same')(input_layer)
x = MaxPooling2D((2, 2), padding='same')(x)
x = Conv2D(16, (3, 3), activation='relu', padding='same')(x)
x = MaxPooling2D((2, 2), padding='same')(x)
x = Conv2D(8, (3, 3), activation='relu', padding='same')(x)
encoded = x

x = Conv2D(8, (3, 3), activation='relu', padding='same')(encoded)
x = UpSampling2D((2, 2))(x)
x = Conv2D(16, (3, 3), activation='relu', padding='same')(x)
x = UpSampling2D((2, 2))(x)
x = Conv2D(8, (3, 3), activation='relu', padding='same')(x)
x = Conv2D(1, (3, 3), activation='sigmoid', padding='same')(x)
decoded = x

matrix_reconstructor = Model(input_layer, decoded)
matrix_reconstructor.summary()
matrix_reconstructor.compile(loss='mean_squared_error', optimizer='adam')

matrix_reconstructor.fit(input_data, input_data, epochs=30, batch_size=1000, shuffle=True)

matrix_reconstructor.save('matrix_reconstructor.h5')

reconstructed = matrix_reconstructor.predict(input_data)



print(input_data[0])
print(reconstructed[0])

