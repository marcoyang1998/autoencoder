import tensorflow as tf
from keras.datasets import mnist
import matplotlib.pyplot as plt
import numpy as np
from keras.models import Model,load_model
from keras.layers import Dense, Flatten, Input, Conv2D, Conv2DTranspose, MaxPooling2D, UpSampling2D, BatchNormalization, Reshape
from PIL import Image

(x_train, y_train), (x_test, y_test) = mnist.load_data()
sample_with_noise = []
for index in range(10000):
    random_normal = np.random.normal(0, 20, [28, 28])
    new_pic = x_test[index] + random_normal
    sample_with_noise.append(new_pic)

sample_with_noise = np.array(sample_with_noise).astype('float32')

plt.subplot(221)
plt.imshow(sample_with_noise[0]/255.)
plt.gray()

plt.subplot(223)
plt.imshow(sample_with_noise[1]/255.)
plt.gray()

#plt.show()

denoiser = load_model('denoiser.n5')

pic1 = denoiser.predict(sample_with_noise[0].reshape([-1, 28, 28, 1]).astype('float32'))
pic2 = denoiser.predict(sample_with_noise[1].reshape([-1, 28, 28, 1]).astype('float32'))
plt.subplot(222)
plt.imshow(pic1.reshape([28, 28])*255.)
plt.gray()
plt.subplot(224)
plt.imshow(pic2.reshape([28, 28])*255.)
plt.gray()
plt.show()