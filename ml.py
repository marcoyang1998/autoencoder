import tensorflow as tf
import matplotlib.pyplot as plt
import scipy.misc
from keras.datasets import mnist
from keras.models import Model, Sequential
from tensorflow import keras
from keras.layers import Dense, Flatten, Reshape, Input, Conv2D, Conv2DTranspose, MaxPooling2D, UpSampling2D, BatchNormalization
from PIL import Image

img_x, img_y = 28, 28
input_shape = (img_x, img_y, 1)
(x_train, y_train), (x_test, y_test) = mnist.load_data()  # x: image  y: corresponding label

x_train = x_train.reshape(x_train.shape[0], 28, 28, 1)
x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)

x_train = x_train.astype('float32')/255.
x_test = x_test.astype('float32')/255.

autoencoder = Sequential()

autoencoder.add(Conv2D(32, kernel_size=3, activation='relu', padding='same'))
autoencoder.add(MaxPooling2D((2, 2), padding='same'))
autoencoder.add(Conv2D(16, kernel_size=3, activation='relu', padding='same'))
autoencoder.add(MaxPooling2D((2, 2), padding='same'))
autoencoder.add(Flatten())
autoencoder.add(Dense(1024, activation='relu'))
autoencoder.add(Dense(7*7*16, activation='relu'))
autoencoder.add(Reshape([7, 7, 16]))
autoencoder.add(Conv2D(16, kernel_size=3, activation='relu', padding='same'))
autoencoder.add(UpSampling2D((2, 2)))
autoencoder.add(Conv2D(32, kernel_size=3, activation='relu', padding='same'))
autoencoder.add(UpSampling2D((2, 2)))
autoencoder.add(Conv2D(1, kernel_size=3, activation='sigmoid', padding='same'))

autoencoder.compile(loss='binary_crossentropy', optimizer='adam')
autoencoder.fit(x_train, x_train,
                epochs=40,
                batch_size=1000,
                shuffle=True,
                validation_data=(x_test, x_test))
autoencoder.summary()


decoded_imgs = autoencoder.predict(x_test)

autoencoder.save('cnn_dense_autoencoder.h5')

n = 10
plt.figure(figsize=(20, 4))
for i in range(n):
    # display original
    ax = plt.subplot(2, n, i + 1)
    plt.imshow(x_test[i].reshape(28, 28))
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

    # display reconstruction
    ax = plt.subplot(2, n, i + 1 + n)
    plt.imshow(decoded_imgs[i].reshape(28, 28))
    plt.gray()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
plt.show()